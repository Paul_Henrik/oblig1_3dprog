#include "house.h"
#include <QDebug>
#include "vertex.h"

house::house(float x)
{
  size = x;
}

house::~house()
{

}

void house::initVertexBufferObjects()
{
    float a;
    a = size;
    int b = -5;
    Vertex vertices[16];
    //front left plane
    vertices[0].set_xyz(-3*a,(5*a)+b,2*a);
    vertices[1].set_xyz(-3*a,(0*a)+b,2*a);
    vertices[2].set_xyz(-1*a,(0*a)+b,2*a);
    vertices[3].set_xyz(-1*a,(5*a)+b,2*a);
    vertices[4].set_xyz(-1*a,(3*a)+b,2*a);
    vertices[5].set_xyz(1*a,(3*a)+b,2*a);
    vertices[6].set_xyz(1*a,(5*a)+b,2*a);
    vertices[7].set_xyz(1*a,(0*a)+b,2*a);
    vertices[8].set_xyz(3*a,(0*a)+b,2*a);
    vertices[9].set_xyz(3*a,(5*a)+b,2*a);
    vertices[10].set_xyz(3*a,(7*a)+b,0*a);
    vertices[11].set_xyz(3*a,(5*a)+b,-2*a);
    vertices[12].set_xyz(3*a,(0*a)+b,-2*a);
    vertices[13].set_xyz(-3*a,(7*a)+b,0*a);
    vertices[14].set_xyz(-3*a,(5*a)+b,-2*a);
    vertices[15].set_xyz(-3*a,(0*a)+b,-2*a);

    vertices[0].set_rgb(1,0,0);
    vertices[1].set_rgb(1,0,0);
    vertices[2].set_rgb(1,0,0);
    vertices[3].set_rgb(1,0,0);
    vertices[4].set_rgb(1,0,0);
    vertices[5].set_rgb(0,1,0);
    vertices[6].set_rgb(0,1,0);
    vertices[7].set_rgb(0,1,0);
    vertices[8].set_rgb(0,1,0);
    vertices[9].set_rgb(0,1,0);
    vertices[11].set_rgb(0,0,1);
    vertices[12].set_rgb(0,0,1);
    vertices[13].set_rgb(0,0,1);
    vertices[14].set_rgb(0,0,1);
    vertices[15].set_rgb(0,0,1);

    GLubyte indices[] =
    {
        0,1,2,
        2,3,0,
        3,4,5,
        5,6,3,
        6,7,8,
        8,9,6,
        8,9,12,
        9,8,11,
        11,12,9,
        10,9,12,
        14,15,12,
        12,11,14,
        14,15,1,
        1,0,14,
        14,0,13,
        13,0,9,
        9,10,13,
        13,14,11,
        11,10,13
    };

    initializeOpenGLFunctions();
    // Skal nå sende all vertex og color data til ett buffer
    glGenBuffers(1, &m_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, 16*sizeof(Vertex), vertices, GL_STATIC_DRAW);

    glGenBuffers(1, &m_indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 57*sizeof(GLubyte), indices,
                 GL_STATIC_DRAW);
}

void house::draw(GLint positionAttribute, GLint colorAttribute, GLint textureAttribute)
{
    initializeOpenGLFunctions();
    // Har en array av strukturer (4 Vertex objekter) som skal splittes på to
    // attributter i vertex shader. stride blir her størrelsen av hver struktur
    // Må bruke offset for start
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    glVertexAttribPointer(positionAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0); /// stride 3*sizeof(GL_FLOAT) går også bra!?

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);

    /// Peker til color
    int offset = 3*sizeof(GLfloat);
    glVertexAttribPointer(colorAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<const void*> (offset)); /// stride 4*sizeof(GL_FLOAT) går også bra!?

    offset = 0*sizeof(GLubyte);
    const void* castedOffset = reinterpret_cast<const void*>(offset);
    glDrawElements(GL_TRIANGLES, 57, GL_UNSIGNED_BYTE, castedOffset);
}

void house::move(GLfloat dt)
{

}

