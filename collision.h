#ifndef COLLISION_H
#define COLLISION_H


class GameObject;
class Collision
{
public:
Collision();
bool detect(GameObject* o1, GameObject* o2);
void perform(GameObject* o1, GameObject* o2);
};

#endif // COLLISION_H
