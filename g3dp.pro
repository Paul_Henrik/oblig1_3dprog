RESOURCES += \
    shadere.qrc

DISTFILES += \
    fragmentshader.glsl \
    vertexshader.glsl \
    Hund2.bmp

HEADERS += \
    gameobject.h \
    gamewidget.h \
    mainWindow.h \
    qopenglfunctions.h \
    shaderprogram.h \
    tetraeder.h \
    texture.h \
    vec3.h \
    vertex.h \
    xyz.h \
    cube.h \
    wheel.h \
    OktaederBall.h \
    plane.h \
    camera.h \
    collision.h \
    house.h \
    fence.h

SOURCES += \
    gameobject.cpp \
    gamewidget.cpp \
    main.cpp \
    mainwindow.cpp \
    shaderprogram.cpp \
    tetraeder.cpp \
    texture.cpp \
    vertex.cpp \
    xyz.cpp \
    cube.cpp \
    wheel.cpp \
    OktaederBall.cpp \
    plane.cpp \
    camera.cpp \
    collision.cpp \
    house.cpp \
    fence.cpp

QT+= opengl
