#ifndef FENCE_H
#define FENCE_H

#include "gameobject.h"


class fence : public GameObject
{
public:
    fence(float size, float height, float yaxis);
    ~fence();
    void initVertexBufferObjects();
    void draw(GLint positionAttribute, GLint colorAttribute, GLint textureAttribute=-1);
     virtual void move(GLfloat dt=0.017);
private:
    float a, b, h;
};

#endif // FENCE_H
