#include "collision.h"
#include "GameObject.h"
#include "vec3.h"

Collision::Collision()
{
}
bool Collision::detect(GameObject* o1, GameObject* o2)
{
    Vec3 d = o1->getPosition()-o2->getPosition();
    if (d.length() <= o1->getRadius()+o2->getRadius())
    {
        //qDebug() << o1->getPosition().x;
        //qDebug() << d.length();
        return true;
    }
    return false;
}
void Collision::perform(GameObject* o1, GameObject* o2)
{
    //qDebug() << "hit";
    Vec3 v = o1->getVelocity();
    qDebug() << o1->getVelocity().x;;
    o1->setVelocity(o2->getVelocity());
    o2->setVelocity(v);
}

