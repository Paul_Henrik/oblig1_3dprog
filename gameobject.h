#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H

#include <QtGui>
#include <QOpenGLWidget>
#include <QtGui/QOpenGLFunctions>
#include "vec3.h"

class Vertex;
//! \brief Base class for displayable objects.

//! This class includes a couple of virtual functions and a pointer
//! for memory allocation.
//! The implementation and also memory allocation is done in derived classes.
//!
class GameObject : protected QOpenGLFunctions//public QOpenGLWidget, protected QOpenGLFunctions
{
public:
    GameObject();

    //! \brief virtual destructor.

    //! The destructor make the OpenGL calls
    //! glDeleteBuffers(1, &m_vertexBuffer);
    //! glDeleteBuffers(1, &m_indexBuffer);
    //! before the object itself is deleted.
    virtual ~GameObject();
    virtual void initVertexBufferObjects() = 0;
    virtual void draw(GLint positionAttribute, GLint colorAttribute, GLint textureAttribute=-1) = 0 ;
    virtual void setTexture(GLuint texture);
    virtual void setPosition(Vec3 position) {m_position = position;}
    virtual Vec3 getPosition() {return m_position;}
    virtual void setVelocity(Vec3 velocity) {m_velocity = velocity;}
    virtual Vec3 getVelocity() {return m_velocity;}
    virtual float setRotation(float rotasjon) { m_rotasjon = rotasjon; }
    virtual float getRotation() { return m_rotasjon; }
    virtual void move(GLfloat dt=0.017) = 0; //Flytte objektet for hver ny frame, slik at ny posisjon er klar til neste frame.
    virtual float getRadius() { return m_radius; }

    QMatrix4x4 getMatrix();


protected:
    Vertex* m_vertices;         // minneallokering i avledede klasser
    GLuint m_antallVertices;
    GLuint m_vertexBuffer;      //GLuint m_normalBuffer; //GLuint m_textureBuffer;
    GLuint m_indexBuffer;
    GLuint m_texture;
    Vec3 m_position;
    Vec3 m_velocity;
    float m_radius;
    float m_rotasjon;
    QMatrix4x4 m_matrix4x4;
private:
};

#endif // GAMEOBJECT_H
