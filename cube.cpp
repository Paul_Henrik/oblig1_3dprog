#include "cube.h"
#include <QDebug>
#include "vertex.h"


cube::cube() : GameObject()
{

}


cube::~cube()
{

}
void cube::initVertexBufferObjects()
{

    Vertex vertices[36];
    double a = 1;
//front

    vertices[0].set_xyz(-a, a, a);
    vertices[1].set_xyz(-a, -a, a);
    vertices[2].set_xyz(a, -a, a);
    vertices[3].set_xyz(-a, a, a);
    vertices[4].set_xyz(a, a, a);
    vertices[5].set_xyz(a, -a, a);
//back
    vertices[6].set_xyz(-a, a, -a);
    vertices[7].set_xyz(-a, -a, -a);
    vertices[8].set_xyz(a, -a, -a);
    vertices[9].set_xyz(-a, a, -a);
    vertices[10].set_xyz(a, a, -a);
    vertices[11].set_xyz(a, -a, -a);
//right side
    vertices[12].set_xyz(a, a, a);
    vertices[13].set_xyz(a, -a, a);
    vertices[14].set_xyz(a, a, -a);
    vertices[15].set_xyz(a, -a, a);
    vertices[16].set_xyz(a, -a, -a);
    vertices[17].set_xyz(a, a, -a);
//left side
    vertices[18].set_xyz(-a, a,  a);
    vertices[19].set_xyz(-a, -a, a);
    vertices[20].set_xyz(-a, a, -a);
    vertices[21].set_xyz(-a, -a, a);
    vertices[22].set_xyz(-a, -a, -a);
    vertices[23].set_xyz(-a, a, -a);
//top
    vertices[24].set_xyz(-a, a, a);
    vertices[25].set_xyz(-a, a, -a);
    vertices[26].set_xyz(a, a, -a);
    vertices[27].set_xyz(a, a, -a);
    vertices[28].set_xyz(a, a, a);
    vertices[29].set_xyz(-a, a, a);
//bottom
    vertices[30].set_xyz(-a, -a, a);
    vertices[31].set_xyz(-a, -a, -a);
    vertices[32].set_xyz(a, -a, -a);
    vertices[33].set_xyz(a, -a, -a);
    vertices[34].set_xyz(a, -a, a);
    vertices[35].set_xyz(-a, -a, a);
//Colors
//front
    vertices[0].set_rgb(1.0, 0.0, 0.0);
    vertices[1].set_rgb(1.0, 0.0, 0.0);
    vertices[2].set_rgb(1.0, 0.0, 0.0);
    vertices[3].set_rgb(1.0, 0.0, 0.0);
    vertices[4].set_rgb(1.0, 0.0, 0.0);
    vertices[5].set_rgb(1.0, 0.0, 0.0);
//back
    vertices[6].set_rgb(0.0, 1.0, 0.0);
    vertices[7].set_rgb(0.0, 1.0, 0.0);
    vertices[8].set_rgb(0.0, 1.0, 0.0);
    vertices[9].set_rgb(0.0, 1.0, 0.0);
    vertices[10].set_rgb(0.0, 1.0, 0.0);
    vertices[11].set_rgb(0.0, 1.0, 0.0);
//right
    vertices[12].set_rgb(0.0, 0.0, 1.0);
    vertices[13].set_rgb(0.0, 0.0, 1.0);
    vertices[14].set_rgb(0.0, 0.0, 1.0);
    vertices[15].set_rgb(0.0, 0.0, 1.0);
    vertices[16].set_rgb(0.0, 0.0, 1.0);
    vertices[17].set_rgb(0.0, 0.0, 1.0);
//left
    vertices[18].set_rgb(1.0, 1.0, 0.0);
    vertices[19].set_rgb(1.0, 1.0, 0.0);
    vertices[20].set_rgb(1.0, 1.0, 0.0);
    vertices[21].set_rgb(1.0, 1.0, 0.0);
    vertices[22].set_rgb(1.0, 1.0, 0.0);
    vertices[23].set_rgb(1.0, 1.0, 0.0);
//top
    vertices[24].set_rgb(0.0, 1.0, 1.0);
    vertices[25].set_rgb(0.0, 1.0, 1.0);
    vertices[26].set_rgb(0.0, 1.0, 1.0);
    vertices[27].set_rgb(0.0, 1.0, 1.0);
    vertices[28].set_rgb(0.0, 1.0, 1.0);
    vertices[29].set_rgb(0.0, 1.0, 1.0);
//bottom
    vertices[30].set_rgb(0.0, 0.0, 0.0);
    vertices[31].set_rgb(0.0, 0.0, 0.0);
    vertices[32].set_rgb(0.0, 0.0, 0.0);
    vertices[33].set_rgb(0.0, 0.0, 0.0);
    vertices[34].set_rgb(0.0, 0.0, 0.0);
    vertices[35].set_rgb(0.0, 0.0, 0.0);

    initializeOpenGLFunctions();

    // Skal nå sende all vertex og color data til ett buffer
    glGenBuffers(1, &m_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);

    glBufferData(GL_ARRAY_BUFFER, 36*sizeof(Vertex), vertices, GL_STATIC_DRAW);
}

void cube::draw(GLint positionAttribute, GLint colorAttribute, GLint textureAttribute)
{
    initializeOpenGLFunctions();
    // Har en array av strukturer (4 Vertex objekter) som skal splittes på to
    // attributter i vertex shader. stride blir her størrelsen av hver struktur
    // Må bruke offset for start
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    glVertexAttribPointer(positionAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0); /// stride 3*sizeof(GL_FLOAT) går også bra!?

    /// Peker til color
    int offset = 3*sizeof(GLfloat);
    glVertexAttribPointer(colorAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<const void*> (offset)); /// stride 4*sizeof(GL_FLOAT) går også bra!?

    glDrawArrays(GL_TRIANGLES, 0/*tegner alle punktene fra 0*/, 36/*til tre*/); // Nytt

}

void cube::move(GLfloat dt)
{

}
