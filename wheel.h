#ifndef WHEEL_H
#define WHEEL_H
#include "gameobject.h"


class wheel : public GameObject
{
public:
    wheel(float px= -1.0f, float vx=1.0f);
    ~wheel();
    void initVertexBufferObjects();
    void draw(GLint positionAttribute, GLint colorAttribute, GLint textureAttribute=-1);
    void move(float dt=0.017);
    float getRotation() { return m_rotasjon; }
private:
    //Vec3 m_posisjon;
    //Vec3 m_speed;
    //float m_radius;
};

#endif // WHEEL_H
