#ifndef TETRAEDER_H
#define TETRAEDER_H

#include "gameobject.h"

class Tetraeder : public GameObject
{
private:

public:
    Tetraeder();
    ~Tetraeder();
    void initVertexBufferObjects();
    void draw(GLint positionAttribute, GLint colorAttribute, GLint textureAttribute=-1);
    virtual void move(GLfloat dt=0.017);
};

#endif // TETRAEDER_H
