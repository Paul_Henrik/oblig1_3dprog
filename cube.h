#ifndef CUBE_H
#define CUBE_H

#include "gameobject.h"

class cube : public GameObject
{
private:

public:
    cube();
    ~cube();
    void initVertexBufferObjects();
    void draw(GLint positionAttribute, GLint colorAttribute, GLint textureAttribute=-1);
    virtual void move(GLfloat dt=0.017);


};

#endif // CUBE_H
