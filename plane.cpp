#include "plane.h"
#include <QDebug>
#include "vertex.h"
plane::plane() : GameObject()
{

}

plane::~plane()
{

}



void plane::initVertexBufferObjects()
{
    int a, y;
    a = 4;
    y = -2;
    Vertex vertices[4];
    vertices[0].set_xyz(a, y, -a);
    vertices[1].set_xyz(a, y, a);
    vertices[2].set_xyz(-a, y, a);
    vertices[3].set_xyz(-a, y, -a);
    vertices[0].set_rgb(0.3,0,0.0);
    vertices[1].set_rgb(0.3,0,0.5);
    vertices[2].set_rgb(0.3,0,0.5);
    vertices[3].set_rgb(0.3,0,0.5);

    GLubyte indices[] =
    {
        0,1,2,
        0,3,2
    };

    initializeOpenGLFunctions();
    // Skal nå sende all vertex og color data til ett buffer
    glGenBuffers(1, &m_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, 4*sizeof(Vertex), vertices, GL_STATIC_DRAW);

    glGenBuffers(1, &m_indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 6*sizeof(GLubyte), indices,
                 GL_STATIC_DRAW);
}

void plane::draw(GLint positionAttribute, GLint colorAttribute, GLint textureAttribute)
{
    initializeOpenGLFunctions();
    // Har en array av strukturer (4 Vertex objekter) som skal splittes på to
    // attributter i vertex shader. stride blir her størrelsen av hver struktur
    // Må bruke offset for start
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    glVertexAttribPointer(positionAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0); /// stride 3*sizeof(GL_FLOAT) går også bra!?

    /// Peker til color
    int offset = 3*sizeof(GLfloat);
    glVertexAttribPointer(colorAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<const void*> (offset)); /// stride 4*sizeof(GL_FLOAT) går også bra!?

    offset = 0*sizeof(GLubyte);
    const void* castedOffset = reinterpret_cast<const void*>(offset);
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_BYTE, castedOffset);


    //glDrawArrays(GL_TRIANGLES, 0, 18); // Nytt

}

void plane::move(GLfloat dt)
{

}
