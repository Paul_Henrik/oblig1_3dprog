#ifndef PLANE_H
#define PLANE_H

#include "gameobject.h"

class plane : public GameObject
{
public:
    plane();
    ~plane();
    void initVertexBufferObjects();
    void draw(GLint positionAttribute, GLint colorAttribute, GLint textureAttribute=-1);
     virtual void move(GLfloat dt=0.017);
};

#endif // PLANE_Hc

