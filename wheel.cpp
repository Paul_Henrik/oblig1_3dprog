#include "wheel.h"
#include <QDebug>
#include "vertex.h"
#include <cmath>
wheel::wheel(float px, float vx) : GameObject()
{
    m_position.x = px;
    m_velocity.x = vx;
    m_velocity.z = 0.0;
    m_radius = 1.0;
}

wheel::~wheel()
{

}

//void wheel::move(GLfloat dt)
//{
//    // Fartsligningen v=s/t eller s=v*t gjelder i alle koordinater
//    // Tiden fra forrige frame er dt
//    // Forflytningen blir v*dt
//    // Gjelder i hver koordinat/retning
//    m_position = m_position + m_hastighet * dt;
//}



void wheel::initVertexBufferObjects()
{

    Vertex vertices[26];
    float sektor = M_PI / 12;
    for(int i = 1;i<26;i++)
    {
        float t = i * sektor;
        float x = cos(t);
        float y = sin(t);
        float z = 0;
        vertices[i].set_xyz(x, y, z);
        vertices[i].set_rgb(1,0,0);
    }
    vertices[0].set_xyz(0,0,0);
    vertices[0].set_rgb(0,0,1);
    vertices[1].set_rgb(0,1,0);

    GLubyte indices[] =
    {
        0,1,2,
        0,2,3,
        0,3,4,
        0,4,5,
        0,5,6,
        0,6,7,
        0,7,8,
        0,8,9,
        0,9,10,
        0,10,11,
        0,11,12,
        0,12,13,
        0,13,14,
        0,14,15,
        0,15,16,
        0,16,17,
        0,17,18,
        0,18,19,
        0,19,20,
        0,20,21,
        0,21,22,
        0,22,23,
        0,23,24,
        0,24,25,
        0,25,1
    };

        initializeOpenGLFunctions();
    // Skal nå sende all vertex og color data til ett buffer
    glGenBuffers(1, &m_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, 26*sizeof(Vertex), vertices, GL_STATIC_DRAW);

    glGenBuffers(1, &m_indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);

    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 75*sizeof(GLubyte), indices,
                 GL_STATIC_DRAW);
}

void wheel::draw(GLint positionAttribute, GLint colorAttribute, GLint textureAttribute)
{
    initializeOpenGLFunctions();
    // Har en array av strukturer (4 Vertex objekter) som skal splittes på to
    // attributter i vertex shader. stride blir her størrelsen av hver struktur
    // Må bruke offset for start
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    glVertexAttribPointer(positionAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0); /// stride 3*sizeof(GL_FLOAT) går også bra!?

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
    /// Peker til color
    int offset = 3*sizeof(GLfloat);
    glVertexAttribPointer(colorAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<const void*> (offset)); /// stride 4*sizeof(GL_FLOAT) går også bra!?

    offset = 0*sizeof(GLubyte);
    const void* castedOffset = reinterpret_cast<const void*>(offset);
    glDrawElements(GL_TRIANGLES, 75, GL_UNSIGNED_BYTE, castedOffset);


    //glDrawArrays(GL_TRIANGLES, 0, 18); // Nytt

}

void wheel::move(float dt)
{
    // Fartsligningen v=s/t eller s=v*t gjelder i alle koordinater
    // Tiden fra forrige frame er dt
    // Forflytningen blir v*dt
    // Gjelder i hver koordinat/retning
    m_position = m_position + m_velocity * dt;
    // Etter definisjonen på radianer har vi teta = s/r => dteta = ds/r
    // ds = r * dteta
    // Hvor langt har hjulet rullet på en frame? Vi må måle avstanden i den
    // retningen hjulet beveger seg med en length() funksjon:
    // float s = m_hastighet.length()*dt;
    // Lag en funksjon Vec3::length() eller
    float s = sqrt(m_velocity.x*m_velocity.x+m_velocity.y*m_velocity.y+m_velocity.z* m_velocity.z)*dt;
    // Rotasjon i radianer
    float r = s/m_radius;
    // Rotasjon i grader
    r = r * 180 / M_PI;
    m_rotasjon += r;

    // Lager en matrise av dette
    m_matrix4x4.setToIdentity();
    m_matrix4x4.translate(m_position.x, m_position.y, m_position.z);
    Vec3 n(0.0, 1.0, 0.0);
    Vec3 v_rot = n^m_velocity;
    v_rot.normalize();
    m_matrix4x4.rotate(m_rotasjon, v_rot.x, v_rot.y, v_rot.z);
}
