#ifndef HOUSE_H
#define HOUSE_H
#include "gameobject.h"

class house : public GameObject
{
public:
    house(float x);
    ~house();
    void initVertexBufferObjects();
    void draw(GLint positionAttribute, GLint colorAttribute, GLint textureAttribute=-1);
    virtual void move(GLfloat dt=0.017);
private:
    float size;
};

#endif // HOUSE_H
