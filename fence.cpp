#include "fence.h"
#include <QDebug>
#include "vertex.h"

fence::fence(float size, float height, float yaxis)
{
    a = size;
    b = yaxis;
    h = height;
}

fence::~fence()
{

}

void fence::initVertexBufferObjects()
{
    Vertex vertices[8];
    vertices[0].set_xyz(-a,b+h,a);
    vertices[1].set_xyz(-a,b,a);
    vertices[2].set_xyz(a,b,a);
    vertices[3].set_xyz(a,b+h,a);
    vertices[4].set_xyz(a,b+h,-a);
    vertices[5].set_xyz(a,b,-a);
    vertices[6].set_xyz(-a,b,-a);
    vertices[7].set_xyz(-a,b+h,-a);




    vertices[0].set_rgb(1,0,0);
    vertices[1].set_rgb(1,0,0);
    vertices[2].set_rgb(0,1,0);
    vertices[3].set_rgb(0,1,0);
    vertices[4].set_rgb(0,0,1);
    vertices[5].set_rgb(0,0,1);
    vertices[6].set_rgb(1,1,1);
    vertices[7].set_rgb(1,1,1);

    GLubyte indices[] =
    {
        0, 3, 2,
        0, 2, 1,

        3, 4, 5,
        3, 5, 2,

        4, 7, 6,
        4, 6, 5,

        7, 0, 1,
        7, 1, 6

//        4,5,6,
//        6,7,4,
//        6,7,0,
//        0,1,6
    };

    initializeOpenGLFunctions();
    // Skal nå sende all vertex og color data til ett buffer
    glGenBuffers(1, &m_vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER,8*sizeof(Vertex), vertices, GL_STATIC_DRAW);

    glGenBuffers(1, &m_indexBuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, 8 * 3 * sizeof(GLubyte), indices,
                 GL_STATIC_DRAW);
}

void fence::draw(GLint positionAttribute, GLint colorAttribute, GLint textureAttribute)
{
    initializeOpenGLFunctions();
    // Har en array av strukturer (4 Vertex objekter) som skal splittes på to
    // attributter i vertex shader. stride blir her størrelsen av hver struktur
    // Må bruke offset for start
    glBindBuffer(GL_ARRAY_BUFFER, m_vertexBuffer);
    glVertexAttribPointer(positionAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0); /// stride 3*sizeof(GL_FLOAT) går også bra!?

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_indexBuffer);

    /// Peker til color
    int offset = 3*sizeof(GLfloat);
    glVertexAttribPointer(colorAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<const void*> (offset)); /// stride 4*sizeof(GL_FLOAT) går også bra!?

    offset = 0*sizeof(GLubyte);
    const void* castedOffset = reinterpret_cast<const void*>(offset);
    glDrawElements(GL_TRIANGLES, 24, GL_UNSIGNED_BYTE, castedOffset);


    //glDrawArrays(GL_TRIANGLES, 0, 18); // Nytt
}

void fence::move(GLfloat dt)
{

}

